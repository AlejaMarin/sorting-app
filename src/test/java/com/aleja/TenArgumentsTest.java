package com.aleja;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(value = Parameterized.class)
public class TenArgumentsTest {

    private String[] args;
    private int[] expected;

    public TenArgumentsTest(String[] args, int[] expected) {
        this.args = args;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}, new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}},
                {new String[]{"10", "9", "8", "7", "6", "5", "4", "3", "2", "1"}, new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}},
                {new String[]{"5", "10", "3", "8", "1", "7", "2", "9", "4", "6"}, new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}},
                {new String[]{"2", "4", "6", "8", "10", "1", "3", "5", "7", "9"}, new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}}
        });
    }

    @Test
    public void testSortingApp() {
        assertArrayEquals(expected, App.parseAndSortArguments(args));
    }

}
