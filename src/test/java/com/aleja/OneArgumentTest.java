package com.aleja;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(value = Parameterized.class)
public class OneArgumentTest {

    private String[] args;
    private int[] expected;

    public OneArgumentTest(String[] args, int[] expected) {
        this.args = args;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[]{"1"}, new int[]{1}},
                {new String[]{"3"}, new int[]{3}},
                {new String[]{"2"}, new int[]{2}},
                {new String[]{"10"}, new int[]{10}}
        });
    }

    @Test
    public void testSortingApp() {
        assertArrayEquals(expected, App.parseAndSortArguments(args));
    }

}
