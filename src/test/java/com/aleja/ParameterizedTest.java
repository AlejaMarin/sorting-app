package com.aleja;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(value = Parameterized.class)
public class ParameterizedTest {

    private String[] args;
    private int[] expected;

    public ParameterizedTest(String[] args, int[] expected) {
        this.args = args;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[]{"3", "2", "1"}, new int[]{1, 2, 3}},
                {new String[]{"2", "8", "4", "6"}, new int[]{2, 4, 6, 8}},
                {new String[]{"3", "7", "1", "5"}, new int[]{1, 3, 5, 7}},
                {new String[]{"5", "10", "1", "20", "30", "15", "25"}, new int[]{1, 5, 10, 15, 20, 25, 30}}
        });
    }

    @Test
    public void testSortingApp() {
        assertArrayEquals(expected, App.parseAndSortArguments(args));
    }

}
