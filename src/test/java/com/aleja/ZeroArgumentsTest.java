package com.aleja;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(value = Parameterized.class)
public class ZeroArgumentsTest {

    private String[] args;

    public ZeroArgumentsTest(String[] args) {
        this.args = args;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[]{}},
                {new String[]{}},
                {new String[]{}},
                {new String[]{}}
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSortingApp() {
        App.parseAndSortArguments(args);
    }

}
