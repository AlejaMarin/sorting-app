package com.aleja;

import java.util.Arrays;

/**
 * Sorting App Project
 */
public class App {

    public static void main(String[] args) {

        int[] numbers = parseAndSortArguments(args);

        System.out.print("Sorted numbers: ");
        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i]);
            if (i < numbers.length - 1) {
                System.out.print(" ");
            }
        }
        System.out.println();

    }

    public static int[] parseAndSortArguments(String[] args) {

        if (args.length == 0) {
            throw new IllegalArgumentException("No arguments provided.");
        }

        if (args.length > 10) {
            throw new IllegalArgumentException("More than ten arguments provided.");
        }

        int[] numbers = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            try {
                numbers[i] = Integer.parseInt(args[i]);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("Invalid argument: " + args[i], e);
            }
        }
        Arrays.sort(numbers);
        return numbers;
    }

}
